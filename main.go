package main

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"log"
	"math/big"
	"os"
	"strconv"
)

func HexStringToDeci(hex string) (string, error) {
	val := hex[2:]

	n, err := strconv.ParseUint(val, 16, 32)
	if err != nil {
		return "", err
	}

	n2 := uint32(n)

	return fmt.Sprint(n2), nil
}

func genPublicKey(n string, e int) (rsa.PublicKey, error) {
	bigN := new(big.Int)

	_, ok := bigN.SetString(n, 16)
	if !ok {
		return rsa.PublicKey{}, fmt.Errorf("could not parse")
	}

	pub := rsa.PublicKey{
		N: bigN,
		E: e,
	}

	return pub, nil
}

func writeToDisk(s []byte) error {
	// Open a new file for writing only
	file, err := os.OpenFile(
		"SdCard.key",
		os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
		0o666,
	)

	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.Write(s)
	if err != nil {
		return err
	}

	return nil
}

func enc(msg string) error {
	N := "00FB451D3F45D82B92FC6F243D50441DD75F2DB995842A3D389FC4536A27F42242C9C8DCB4DA0E2573E5CA2" +
		"E5D0A2AD7E790D8A79CAC2DE68BEAF99D21E229A9CF04ABD09D61C8C66C4FE3B32456496305792FF9D2D2198B87B" +
		"FAB2637518C1D3F44D27B93EF2C0ED3379993D04944EB356D4BDE343017CFB13405B403A3D81D2C7B099AE5651CF14DD3C" +
		"BE21C435076F244D9DA8F54DA19BA6301AF1F7DA699E2EBFD9C0BB2778D812E8D9BE66089B2783B9E60FA28FA83CD3B356669BC" +
		"15BC84058FEEE493CCFBE2E13E0B53B01886D47EB75BFC75758A5CFA5A1836E697FD51846578B4BDEDE3A6BD1FE4D49ABAC072AED433" +
		"AC5A19BF94C9F6C7F4D95740EF"

	E := 0o10001

	pub, _ := genPublicKey(N, E)

	encryptedPKCS1v15, err := rsa.EncryptPKCS1v15(rand.Reader, &pub, []byte(msg))
	if err != nil {
		return err
	}

	err = writeToDisk(encryptedPKCS1v15)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	hex := "0xe8902a07"

	deci, err := HexStringToDeci(hex)
	if err != nil {
		log.Fatal(err)
	}

	err = enc(deci)
	if err != nil {
		log.Fatal(err)
	}
}
